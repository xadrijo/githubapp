//
//  GitHubAPIManager.swift
//  GitHubApp
//
//  Created by Jorge Jimenez on 1/26/17.
//  Copyright © 2017 Jorge Jimenez. All rights reserved.
//

import Foundation
import Alamofire

class GitHubAPIManager {
    static let sharedInstance = GitHubAPIManager()
    
    func printPublicGists() -> Void {
        Alamofire.request(GistRouter.getPublic())
            .responseString { response in
                if let receivedString = response.result.value {
                    print(receivedString)
                }
        }
    }
    
    func fetchPublicGists(completionHandler: @escaping (Result<[Gist]>) -> Void) {
        let _ = Alamofire.request(GistRouter.getPublic())
            .responseJSON { response in
                let result = self.gistArrayFromResponse(response: response)
                completionHandler(result)
        }
    }
    
    // MARK: - GitHubAPIManageError
    enum GitHubAPIManageError {
        case network(error: Error)
        case apiProvidedError(reason: String)
        case authCouldNot(reason: String)
        case authLost(reason: String)
        case objectSerialization(reason: String)
    }
    
    private func gistArrayFromResponse(response: DataResponse<Any>) -> Result<[Gist]> {
        guard response.result.error == nil else {
            print(response.result.error!)
            return .failure(GitHubAPIManageError.network(error: response.result.error!) as! Error)
        }
        
        // make sure we got JSON and it's an array
        guard let jsonArray = response.result.value as? [[String: Any]] else {
            print("didn't get array of gist object as JSON from API")
            return .failure(GitHubAPIManageError.objectSerialization(reason: "Did not get JSON dictionary in response") as! Error)
        }
        
        // check for "message" errors in the JSON because this API does that
        if let jsonDictionary = response.result.value as? [String: Any],
            let errorMessage = jsonDictionary["message"] as? String {
            return .failure(GitHubAPIManageError.apiProvidedError(reason: errorMessage) as! Error)
        }
        
        // turn JSON into gists
        var gists = [Gist]()
        for item in jsonArray {
            if let gist = Gist(json: item) {
                gists.append(gist)
            }
        }
        return .success(gists)
    }
    
    func imageFrom(urlString: String, completionHandler: @escaping (UIImage?, Error?) -> Void) {
        let _ = Alamofire.request(urlString)
            .response { dataResponse in
                // use the generic response serializer that return Data
                guard let data = dataResponse.data else {
                    completionHandler(nil, dataResponse.error)
                    return
                }
                let image = UIImage(data: data)
                completionHandler(image, nil)
        }
    }
}
